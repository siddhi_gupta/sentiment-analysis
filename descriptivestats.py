import os
import numpy as np
path = os.getcwd()
arabic = path + '/binary_arabic_reviews.tsv'
mandarin = path + '/bookReviewsMandarine.txt'
english = path + '/English_Reviews.txt'

# with open(arabic) as file:
#     charray = file.read()
#
# splitray = charray.split()
# review = 0
# counter = 0
# arablengths = []
# for i,word in enumerate(splitray):
#     if word == str(review):
#         review += 1
#         arablengths.append(i-counter-2) #the minus 2 is because the review number and rating are included in the txt documents
#         counter = i
#
# arabmean = np.mean(arablengths)
# arabmedian = np.median(arablengths)
# arabstd = np.std(arablengths)
# print('Arabic mean: ' + str(arabmean))
# print('Arabic median: ' + str(arabmedian))
# print('Arabic standard deviation: ' + str(arabstd))

with open(english) as file:
    charray = file.read()

englines = []
counter = 0
for i,char in enumerate(charray):
    if char == '\n':
        englines.append(charray[counter:i])
        counter = i

englengths = []
for i,line in enumerate(englines):
    englengths.append(len(englines[i].split())-1) #minus 1 because the rating is included

engmean = np.mean(englengths)
engmedian = np.median(englengths)
engstd = np.std(englengths)
print('English mean: ' + str(engmean))
print('English median: ' + str(engmedian))
print('English standard deviation: ' + str(engstd))

with open(arabic) as file:
    charray = file.read()

arablines = []
counter = 0
for i,char in enumerate(charray):
    if char == '\n':
        arablines.append(charray[counter:i])
        counter = i

arablengths = []
for i,line in enumerate(arablines):
    arablengths.append(len(arablines[i].split())-2) #the minus 2 is because the review number and rating are included in the txt documents

arabmean = np.mean(arablengths)
arabmedian = np.median(arablengths)
arabstd = np.std(arablengths)
print('Arabic mean: ' + str(arabmean))
print('Arabic median: ' + str(arabmedian))
print('Arabic standard deviation: ' + str(arabstd))

with open(mandarin) as file:
    charray = file.read()

mandlines = []
counter = 0
for i,char in enumerate(charray):
    if char == '\n':
        mandlines.append(charray[counter:i])
        counter = i

mandlengths = []
for i,line in enumerate(mandlines):
    mandlengths.append((len(mandlines[i])-2)/6) #this is a quick way to approximate the amount of words taking disyllabic words as the average and taking 3 bytes to encode each character/syllable meaning 3*2=6 as a dividing factor of the byte string

mandmean = np.mean(mandlengths)
mandmedian = np.median(mandlengths)
mandstd = np.std(mandlengths)
print('Mandarin mean: ' + str(mandmean))
print('Mandarin median: ' + str(mandmedian))
print('Mandarin standard deviation: ' + str(mandstd))
