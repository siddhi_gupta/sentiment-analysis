from sklearn import svm
import numpy as np
import math
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sklearn import linear_model, datasets
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier

# f1 = 'final_features/english_features/english_full_doc2vec_features_test.csv'
# f = 'final_features/english_features/english_full_doc2vec_training_features_full_train.csv'

# f1 = 'final_features/english_features/english_full_word2vec_featurestest.csv'
# f = 'final_features/english_features/english_full_word2vec_training_features_full_train.csv'

# f1 = 'final_features/english_features/english_subset_doc2vec_features_test.csv'
# f = 'final_features/english_features/english_subset_doc2vec_training_features_subset__train.csv'

# f1 = 'final_features/english_features/english_subset_word2vec_featurestest.csv'
# f = 'final_features/english_features/english_subset_word2vec_training_features_subset__train.csv'

# f1 = 'final_features/arabic_features/full_doc2vec_features_test.csv'
# f = 'final_features/arabic_features/full_doc2vec_training_features_full_train.csv'

# f1 = 'final_features/arabic_features/full_word2vec_featurestest.csv'
# f = 'final_features/arabic_features/full_word2vec_training_features_full_train.csv'

# f1 = 'final_features/arabic_features/subset_doc2vec_features_test.csv'
# f = 'final_features/arabic_features/subset_doc2vec_training_features_subset__train.csv'

# f1 = 'final_features/arabic_features/subset_word2vec_featurestest.csv'
# f = 'final_features/arabic_features/subset_word2vec_training_features_subset__train.csv'

# f1 = 'new_mandarin/mandarin_full_doc2vec_test_features.tsv'
# f = 'new_mandarin/50%_mandarin_full_doc2vec_train_features.tsv'

# f1 = 'new_mandarin/mandarin_subset_doc2vec_test_features.tsv'
# f = 'new_mandarin/mandarin_subset_doc2vec_train_features.tsv'

# f1 = 'new_mandarin/mandarin_full_word2vec_test_features.tsv'
# f = 'new_mandarin/50%_mandarin_full_word2vec_train_features.tsv'

# f1 = 'new_mandarin/mandarin_subset_word2vec_test_features.tsv'
# f = 'new_mandarin/mandarin_subset_word2vec_train_features.tsv'

# f1 = '/Users/siddhigupta/sentiment-analysis/full_full_features/english_new_full_doc2vec_test_features.tsv'
# f = '/Users/siddhigupta/sentiment-analysis/full_full_features/english_new_full_doc2vec_train_features.tsv'

# f1 = '/Users/siddhigupta/sentiment-analysis/full_full_features/english_new_full_word2vec_test_features.tsv'
# f = '/Users/siddhigupta/sentiment-analysis/full_full_features/english_new_full_word2vec_train_features.tsv'

# f1 = '/Users/siddhigupta/sentiment-analysis/full_full_features/arabic_new_full_doc2vec_test_features.tsv'
# f = '/Users/siddhigupta/sentiment-analysis/full_full_features/arabic_new_full_doc2vec_train_features.tsv'

# f1 = '/Users/siddhigupta/sentiment-analysis/full_full_features/arabic_new_full_word2vec_test_features.tsv'
# f = '/Users/siddhigupta/sentiment-analysis/full_full_features/arabic_new_full_word2vec_train_features.tsv'

# f1 = '/Users/siddhigupta/sentiment-analysis/full_full_features/mandarin_full_doc2vec_test_features.tsv'
# f = '/Users/siddhigupta/sentiment-analysis/full_full_features/mandarin_full_doc2vec_train_features.tsv'

# f1 = '/Users/siddhigupta/sentiment-analysis/full_full_features/mandarin_full_word2vec_test_features.tsv'
# f = '/Users/siddhigupta/sentiment-analysis/full_full_features/mandarin_full_word2vec_train_features.tsv'

f1 = sys.argv[1] #path to test file
f = sys.argv[2] #path to train file 

train_arrays = []
train_labels = []
test_arrays = []
test_labels = []
with open(f, 'r') as openfileobject:
	for id in openfileobject:
		id = id.rstrip("\n").split("\t")
		labels = id[0]
		id.pop(0)
		ids = []
		for i in id:
			try:
				i = float(i)
				if math.isnan(i) :
					continue
				ids.append(i)
			except:
				continue
		if len(ids) == 300:
			train_labels.append(int(float(labels)))
			train_arrays.append(ids)
print train_arrays
print train_labels
with open(f1, 'r') as openfileobject:
	for id in openfileobject:
		id = id.rstrip("\n").split("\t")
		labels = id[0]
		id.pop(0)
		ids = []
		for i in id:
			try:
				i = float(i)
				if math.isnan(i) :
					continue
				ids.append(i)
			except:
				continue
		if len(ids) == 300:
			test_labels.append(int(float(labels)))
			test_arrays.append(ids)
print test_arrays
print test_labels

# Uncomment for Linear kernal SVM
# X = np.array(train_arrays, dtype=float)
# Z = np.array(test_arrays, dtype=float)
# y = np.array(train_labels, dtype=float)
# #clf = svm.SVC(kernel = 'uncomputed')
# clf = svm.LinearSVC()
# gram = np.dot(X, X.T)
# clf.fit(gram, y) 
# gram_test = np.dot(Z, X.T)
# pred = clf.predict(gram_test)
# print pred
# print 'accuracy score: %0.3f' % accuracy_score(test_labels, pred)

# Logistic Regression
classifier = linear_model.LogisticRegression()
classifier.fit(train_arrays, train_labels) 
predicted = classifier.predict(test_arrays)
print predicted
print classifier.score(test_arrays, test_labels)

# Uncomment for Linear Regression
# classifier = linear_model.LinearRegression()
# classifier.fit(train_arrays, train_labels) 
# predicted = classifier.predict(test_arrays)
# print predicted
# print classifier.score(test_arrays, test_labels)

# Uncomment for Decision Tree
# classifier = tree.DecisionTreeClassifier(criterion='gini')
# classifier.fit(train_arrays, train_labels)
# score = classifier.score(test_arrays, test_labels)
# print score
# predicted = classifier.predict(test_arrays)
# print predicted

# Uncomment for SVM
# classifier = svm.SVC()
# classifier.fit(train_arrays, train_labels)
# score = classifier.score(test_arrays, test_labels)
# print score
# predicted = classifier.predict(test_arrays)
# print predicted

# Uncomment for Naive Bayes
# classifier = GaussianNB()
# classifier.fit(train_arrays, train_labels)
# score = classifier.score(test_arrays, test_labels)
# print score
# predicted = classifier.predict(test_arrays)
# print predicted

# Uncomment for K Neighbors Classifier (kNN)
# classifier = KNeighborsClassifier(n_neighbors = 6)
# classifier.fit(train_arrays, train_labels)
# score = classifier.score(test_arrays, test_labels)
# print score
# predicted = classifier.predict(test_arrays)
# print predicted

# Uncomment for Random Forest
# classifier = RandomForestClassifier()
# classifier.fit(train_arrays, train_labels)
# score = classifier.score(test_arrays, test_labels)
# print score
# predicted = classifier.predict(test_arrays)

print "Negative Reviews Count ", list(predicted).count(0)

print "F1 Score ", (f1_score(test_labels, predicted, average="binary"))
print "Precision ", (precision_score(test_labels, predicted, average="binary"))
print "Recall ", (recall_score(test_labels, predicted, average="binary"))