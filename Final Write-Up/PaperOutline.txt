Overall Paper Goal: Convince Ron & TAs that the work was 1)Difficult 2)Creative 3)Original 4)Well-implemented 5)Well-researched 6)Important

Introduction - Why should I care? -> "motivate the problem that you have worked on"
  Why is this research necessary? -> "Why is this interesting?"
    Bag of words models work pretty well but hit a wall
    They have several problems:
      Just based on count/proximity
      ...
    It is clear (HOW??) that the next major leap must involve some sort of semantic representation
    There has recently been work on semantic representations with very nice properties (king-man+woman=queen)

  What is the problem we are trying to solve? -> "Describe the problem"
    The features that these select (words) differ in their content cross-linguistically
    DEMO:
      He goes to the market
      هو) يذهب للسوق)
      他 去 市场 （ta1 qu4 shi4chang3)
    An examination of the features it selects, while straightforward in English, could significantly affect performance cross-linguistically (and therefore affect its applicability to the rest of the world)
    Morphological complexity (WHAT'S THAT?) provides a measure for info. content within a word
  Where is the knowledge gap that we are filling? -> "Why has this not been done before? Challenging?
    Two major world languages which the system could be applied to differ significantly in morpho. complexity and therefore an examination of how the features it selects (and therefore how much info. (the granularity) each feature contains) is needed before its widespread adoption
    Such a comparison of performance versus morpho. complexity does not exist
  How does the work accomplished in this paper fill the gap? ->
    Luckily these 2 languages are prevalent on the internet and data is rich
    This paper examines the aforementioned unexamined consequences of using this system with the same feature selection across languages and therefore contributes knowledge to how the system performs and would need to be changed for cross-linguistic application


Methods
  Materials - What's the data? -> "identify the source data"
    Where does it come from? -> "a specific corpus that you accessed or collected yourself"
      English scraped from goodreads
      Arabic already in LABR
      Mandarin scraped from douban
    How did we get it? -> "that you collected"
      Put in Jay and Siddhi's spiels
    What did we get? -> "the amount of data, annotations it has"
      Give pos/neg breakdown
      Also give average length/sd here?
    How did we manipulate it? -> "annotations... needed to be added"
      Turning of 5/10 point scale into binary classification
      Cleaning up and ensuring only proper language data got through

  Procedure - What did you do to the data? -> "describe the experimental procedure"
    What did we do in this study (previewed before)? -> "describe the exp. procedure"
    How did we do what we did? ->
      What tools did we use? -> "what methods you use to process the data"
      What tools did we create or change to suit our own needs? -> "algorithms, features, and tools, and any annotations you made"

  Evaluation - How do we measure performance of our system? -> "method for evaluating system's performance"
    How do we get the numbers we get? -> "specific measures you will take"
    What will these numbers be in reference to? -> "the baseline to which you compare your system"

Results - How did it do?
  What are the numbers? -> "report how your system performs"
  Numerically, how do the different condition's numbers compare?
  Numerically, how do they stack up? -> "how it compares to a baseline"
  Why did the system score those numbers?


Discussion - What does this mean?
