\documentclass[10pt, letterpaper]{article}
%\usepackage[margin=1.25in]{geometry}
\usepackage[margin=1.25in]{geometry}
\usepackage{multicol}
\usepackage{multirow}
%\setlength{\textwidth}{39pc}
%\setlength{\textheight}{54pc}
%\setlength{\parindent}{1em}
%\setlength{\parskip}{0pt plus 1pt}
%\setlength{\oddsidemargin}{0pc}
%\setlength{\marginparwidth}{0pc}
%\setlength{\topmargin}{-2.5pc}
%\setlength{\headsep}{20pt}
%\setlength{\columnsep}{1.5pc}
\setlength{\columnsep}{.25in}
\setlength{\headsep}{20pt}
\usepackage{titlesec}
\titlespacing*{\section}
{0pt}{0pt}{6pt}
\titlespacing*{\subsection}
{0pt}{6pt}{6pt}
\usepackage{graphicx}
%\usepackage{pslatex} %Times New Roman font
\usepackage{tipa}
\usepackage{rotating}
\usepackage{tablefootnote}
\usepackage{amsmath}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{sidecap}
\usepackage{longtable}
\usepackage{array}
\sidecaptionvpos{figure}{c}
\newcommand{\degree}{\ensuremath{^\circ}}
%\usepackage{cancel} places slash through text (/) syntax \cancel{}
%\usepackage{subscript} text subscript command syntax \textsubscript{}, TeX's builtin \textsuperscript{} counterpart
\usepackage{covington}
\usepackage{arabtex}
\usepackage{utf8}

\usepackage{moreverb}
\immediate\write18{texcount -inc 
22_Final_Report.tex %this is where the name of the document to be word counted goes
> /tmp/wordcount.tex}
\newcommand\wordcount{\verbatiminput{/tmp/wordcount.tex}}

\setlength{\parskip}{5mm plus2mm minus 1.5mm}
\setlength{\parindent}{1cm}
\begin{document}
\setcode{utf8}
\LARGE
\begin{centering}
\vspace{20pt}
Cross-linguistic Efficacy of Distributed Representations forSentiment Analysis: An Investigation of the Effect of Morphological Complexity

\vspace{20pt}

\begin{table}[!h]
\centering
\large
\begin{tabular}{l l l}
Group Member & E-mail & USC ID \\ \hline
Siddhi Gupta & siddhigu@usc.edu & 6283620337 \\ Joe Hoover & jehoover@usc.edu & 3863661653 \\ Maury Lander-Portnoy & landerpo@usc.edu & 8228712746 \\ Jay Priyadarshi & jpriyada@usc.edu & 9818939924 \\
\end{tabular}
\end{table}

\large

\vspace{20pt}
April 29, 2016

\end{centering}

\normalsize

\section{Introduction}
Sentiment analysis (SA) allows the classification of natural language text according to the author's expressed opinion.  This classification, usually simply positive or negative, is central to numerous applications from product marketing and quality control to predicting political trends and monitoring public order.  Because this classification relies on discrimination and sorting based on features, the selection of features becomes a crucial design problem and can have a great effect on the performance of programs that utilize SA.

Simple ``bag of words" models perform adequately but lose important linguistic information by poorly encoding semantic representation and wholly discarding syntactic structure.\cite{bagofwords}
  In light of these shortcomings, recent strides have been made in improving the performance of classifiers by using distributed representations (DRs) to improve the semantic representations of words and capture syntactic structure.  These DRs %vector-space word representations
have several desirable properties that capture semantic and syntactic aspects of language in vector-space relationships such as those shown below.\cite{vectors}

\begin{subequations}
\begin{centering}
Gender:	$``\vec{King} - \vec{Man} + \vec{Woman}" \approx ``\vec{Queen}"$ \\
Number:	$``\vec{Apple}-\vec{Apples}" \approx ``\vec{Car}-\vec{Cars}"$

\end{centering}
\end{subequations}

While these representations of words hold promise for advancing classification, the amount of information a word contains, and therefore the informational content of each classification feature, differs cross-linguistically.  Because of the importance of feature selection on classifier performance, this difference in the informational content of features may cause performance of the same program %is "program" really the best word here? "algorithm"? "application"?
to differ significantly cross-linguistically.  A recent comparison of DRs' application to German vs.~English provides evidence that the utility of DRs may indeed decrease as features' informational content increases.\cite{german}

An effective measure of the amount of informational content that words of a certain language contain is morphological complexity.  Morphological complexity describes how many atomic units of meaning, morphemes, a single word of a given language typically contains.  A language with high morphological complexity has numerous morphemes per word, and therefore encodes more information in a single word than a language with low morphological complexity. %Do I need a further explanation of this concept of morphemes and morphological complexity and its importance for feature selection and classifier performance or have I beaten the point to death?
Consider the following example of three languages of differing in morphological complexity: Arabic, English, and Mandarin.

\begin{table}[!h]
\begin{tabular}{c c c}
\underline{Mandarin} & \underline{English} & \underline{Arabic} \\
\includegraphics[height=.7in]{MandarinEx}&\includegraphics[height=.35in]{EnglishEx}& \includegraphics[height=.7in]{ArabicEx}\\
\end{tabular}
\caption{The same sentence expressed in three languages of differing morphological complexity.  The languages are presented in order of increasing morphological complexity with the simplest (Mandarin) on the left, the most complex (Arabic) on the right, and one of intermediate complexity (English) in the middle.}
\end{table}


The above languages not only illustrate varying degrees of morphological complexity but are also three of the top four languages spoken worldwide and therefore represent extremely plausible cross-linguistic applications of DRs.  In this paper, we examine a novel application of DRs to these languages aiming to not only probe the theoretically interesting question of the effect of morphological complexity on DRs, but also to provide directly applicable data for the cross-linguistic application of DRs to Mandarin or Arabic.

%Are we still doing data paucity?

While the three languages used in this paper all have ample data on which to train DR classifiers, many languages face the problem of data paucity.  It is currently unclear what effect data paucity has on the performance of SA classifiers utilizing DRs.  To this end, an additional manipulation we perform is to constrain the amount of training data available to the classifier to simulate the effect of data paucity.  This comparison of a classifier trained on a sizable corpus to one trained on a limited corpus can illuminate the viability of DRs for resource deprived languages.



\section{Methods}
\subsection{Materials}\label{materials}
Corpora of the three languages consisted of book reviews accompanied by ratings collected from www.goodreads.com (English and Arabic) and www.read.douban.com
(Mandarin).  The Arabic corpus was already in existence and only required preprocessing to be suitable for the DR generation program.\cite{LABR}  The remaining two, English and Mandarin, were generated from scratch using the Python library \emph{Beautiful Soup} %there's gotta be a better word for that
to scrape equivalent book review sites matching the domain and composition of the existing Arabic corpus.  These data were then cleaned and preprocessed, ensuring correct format and language, to be useable by the same DR generation program used for the Arabic data.  The Mandarin data's ratings were scaled down from their original 10 point scale to match the 5 point scale of the Arabic and English data.  Due to the desire for a binary SA classifier, data were converted from a five point scale to simply positive ($rating>3$) or negative ($rating\leq 3$), which resulted in the proportions found in the table below.

%Further descriptive stats

\begin{table}[!h]
\begin{tabular}{|l|l l l l|}\hline
Language & \# of reviews & \% Positive & \%Negative & Avg. Length \\ \hline
Mandarin & 54,003 & 73.5\% & 26.5\% \\
English & 47,611 & 53.8\% & 46.2\% \\
Arabic & \\\hline
\end{tabular}
\end{table}




\subsection{Procedure}
Both word-level and document-level DR features have been shown to have high information value for SA.\cite{dossantos}\cite{leandmikolov}  Because of this, we generated three different types of feature representations of documents: word-level DRs, hierarchical DRs, and baseline n-grams. %is the tf-idf thing still a thing?
The first of these representation types, word-level DRs, was created by averaging a given document's word level representations generated by Word2Vec. %cite?
The second of these, hierarchical DRs, was created by Doc2Vec with the distributed memory algorithm on a training corpus.
The last of these, n-grams, was created by counting the individual words a document contained.


\subsection{Evaluation}
The performance of the classifier was measured by reserving an unseen portion of the data described in \S~\ref{materials} for testing.  Each review in this test data was labeled with binary positive/negative SA classes and the output of the classifier was compared to this label.  Outcomes were thus either correct or incorrect in their classifying of reviews%this is probably why logistic regression works best btw.
and this same method was used to evaluate all the different manipulations performed in the experiment.  In order to draw inferences about the effect our experimental manipulations had on the performance of the classifier, a baseline n-gram model was also deployed using the same procedure as the DR model.  This baseline served to illustrate the effectiveness of DRs as a function of the target language's morphological complexity and the training corpus size.



\pagebreak
\section{Results}

Here we report on the performance of all the different manipulations of our classifier.
\begin{table}[!h]
\begin{tabular}{l l l l}
& Mandarin & English & Arabic \\
50\% Training Data & & & \\
100\% Training Data & & & \\
N-gram Baseline & & & \\
\end{tabular}
\end{table}

\section{Discussion}



\begin{thebibliography}{1}
\bibitem{bagofwords}
Le and Mikolov "Distributed Representations of Sentences and Documents"
\bibitem{vectors}
Mikolov et al. 2013 "Linguistic Regularities..."
\bibitem{german}
K\"oper, Scheible, \& im Walde, 2015
\bibitem{LABR}
LABR
\bibitem{Soup}
https://www.crummy.com/software/BeautifulSoup/
\bibitem{dossantos}
dos Santos \& Gatti, 2014
\bibitem{leandmikolov}
Le \& Mikolov, 2014
\end{thebibliography}

\appendix
\section{Division of Labor}
\begin{table}[!h]
\begin{tabular}{|l|l|}
\hline
Siddhi & Scraping and cleaning of Mandarin review corpus, Preprocessing of Mandarin reviews, \\& Proposal Write-up, Evaluation? \\
\hline
Joe & Distributed representation training and classification, Preprocessing of Arabic reviews, \\& Proposal Write-up, Evaluation? \\
\hline
Maury & Literature review, Linguistic consulting, Proposal Write-up, Final Write-up, \\& Descriptive Statistics, Infographic Generation? \\
\hline
Jay & Scraping and cleaning of English review corpus, Preprocessing of English reviews,\\& Building n-gram baseline, Evaluation?\\
\hline
\end{tabular}
\end{table}
\section{Wordcount (Excluding References)}
\wordcount





\end{document}