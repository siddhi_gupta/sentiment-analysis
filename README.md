General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Text Wrangler or any other text editor.
3. Have the input file in the required format.

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Proposal](https://drive.google.com/file/d/0B6zp1SiU4EkrUndSeFkyc3J5akk/view?usp=sharing)
* [Final Report](https://drive.google.com/file/d/0B6zp1SiU4EkrYTdqQUVxWExNeXc/view?usp=sharing)

Table of Contents
==============
----------------------------------
[TOC]

Cross-linguistic Efficacy of Distributed Representations for Sentiment Analysis: An investigation of the effects of data
paucity and morphological complexity
================================================================================
--------------------------------------------------------------------------------
Downsampling data and generating features
========================================================================
------------------------------------------------------------------------

To downsample data execute stratified_downsample.py in terminal. The program requires three arguments:

1. The name of the file containing binary reviews. The first column must contain ratings and the second column must contain documents.
2. The number of positive cases to include in the downsampled data
3. The number of negative cases to include in the downsampled data

**For example:**

python stratified_downsample.py binary_arabic_reviews.tsv 12000 4000

To generate features execute feature_pipeline.py in terminal. The program requires two arguments:

1. The language of the data for which features are being generated
2. The name of the file containing binary reviews. The first column must contain ratings and the second column must contain documents.

**For example:**

python feature_pipeline.py arabic stratified_sample_binary_arabic_reviews.tsv

This program will generate a lot of files, must of which you don't need. A pickled dictionary object will also be generated.
The dictionary object will be called \[langage\]_file_names.pickle and it contains key value pairs where values 
are file names and keys express the contents of the files named in the values. 

**The keys are:**

*Subset Data:*

* 'word2vec_subset_test_features': test features inferred from word2vec model trained on 50% of input data
* 'doc2vec_subset_test_features': test features inferred from doc2vec model trained on 50% of input data
* 'word2vec_subset_training_features': training features from word2vec trained on 50% of input data
* 'doc2vec_subset_training_features': training features from doc2vec trained on 50% of input data

*Full Data:*

* 'word2vec_full_test_features': test features inferred from word2vec model trained on 100% of input data
* 'doc2vec_full_test_features':  test features inferred from doc2vec model trained on 100% of input data
* 'word2vec_full_subset_for_classifier_training_features': 50% of training features from word2vec trained on 100% of input data
* 'doc2vec_full_subset_for_classifier_training_features': 50% of training features from doc2vec trained on 100% of input data


Feature Files Guide
========================================================================
------------------------------------------------------------------------

**feature_pipeline.py generates 8 feature files:**

* full_doc2vec_features_test.csv -- Test features for full doc2vec model
* full_doc2vec_training_features_full_train.csv -- Training features for full doc2vec model
* full_word2vec_featurestest.csv -- Test features for full word2vec model
* full_word2vec_training_features_full_train.csv -- Training features for full word2vec model

* subset_doc2vec_features_test.csv -- Test features for subset doc2vec model
* subset_doc2vec_training_features_full_train.csv -- Training features for subset doc2vec model
* subset_word2vec_featurestest.csv -- Test features for subset word2vec model
* subset_word2vec_training_features_full_train.csv -- Training features for subset word2vec model

These files will also have the language name input to feature_pipeline.py appended as prefixes.




Edit Log
=======
-----------------
## Complete Creation
> Date: 29/01/16
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Cross-linguistic Efficacy of Distributed Representations for Sentiment Analysis: An investigation of the effects of data
paucity and morphological complexity
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 17/04/16
> * Joe Hoover: 23/04/16