from bs4 import BeautifulSoup
import urllib2
import itertools
import random
import urlparse
import sys
#28,915
f1 = open ('bookIds_1.txt', 'w')
f2 = open ('bookReviews_1.txt', 'w')

bookIds = []
inputURL = "https://www.goodreads.com/book/popular_by_date/"

year = 1916

while year <= 2016:
	inputURL = "https://www.goodreads.com/book/popular_by_date/"+str(year)+"/"
	res = urllib2.urlopen(inputURL)
	html_code = res.read()
	soup = BeautifulSoup(html_code, 'html.parser')
	bookList = soup.find("div", {"class": "leftContainer"})
	bookItems = bookList.find_all("td", {"width": "100%"})
	for book in bookItems:
		bookIds.append(str(book.find("a",{"class": "bookTitle"}).get("href")))
		f1.write(bookIds[-1] + "\n")
	print bookIds

	year += 1

# really liked it = 4, it was amazing: 5, it was ok: 2, liked it:3, did not like it: 1

rating ={"really liked it":"postive","it was amazing":"positive","it was ok":"negative","liked it": "negative","did not like it": "negative"}
rating_num ={"really liked it":4,"it was amazing":5,"it was ok":2,"liked it": 3,"did not like it": 1}

for id in bookIds:
	try:
		id = id[:-1]
		inputURL = "https://www.goodreads.com" + id
		res = urllib2.urlopen(inputURL)
		html_code = res.read()
		soup = BeautifulSoup(html_code, 'html.parser')
		i = 0
		descDiv = soup.find_all("div", {"class": "reviewText stacked"})
		ratingSpan = soup.find_all("span", {"class": " staticStars"})
		print len(descDiv)
		try:
			for index in range(len(descDiv)):
				temp = descDiv[index].find("span",{"class": "readable"})
				temp1 = temp.find_all("span")
				temp2 = ratingSpan[index].find("span")
				f2.write(id + "			"+((temp1[1].contents)[0]).encode("utf-8") + "		")
				f2.write(rating[((temp2.contents)[0])] + "		"+ ((temp2.contents)[0])+"		" + str(rating_num[((temp2.contents)[0])]) + "\n")
				print ((temp1[1].contents)[0]).encode("utf-8") 
				print rating[((temp2.contents)[0])] + "	"+ ((temp2.contents)[0])+"	" + str(rating_num[((temp2.contents)[0])]) 
		except:
			if (descDiv[index].contents) == "":
				continue
	except:
		continue

