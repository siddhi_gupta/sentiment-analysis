from __future__ import division
__author__ = 'joe'

import pandas as pd
import numpy as np
import sys
import simple_sample
import simple_doc2vec_features
import simple_word2vec_features
import classifier_sample

file_name = sys.argv[1]
n_pos = 39693
n_neg = 14310
seed = 123
language = 'arabic'

test_corpus_name = language + '_test_corpus.tsv'
full_train_corpus_name = language + '_full_train_corpus.tsv'
subset_train_corpus_name = language + '_subset_train_corpus.tsv'


strat_samp = simple_sample.get_stratified_sample(n_pos = n_pos, n_neg = n_neg, file_name = file_name, seed = seed)

simple_sample.train_test_split(data=strat_samp, language=language)

print 'Generating subset model doc2vec features'
simple_doc2vec_features.generate_features(corpus=subset_train_corpus_name,
                                          test_corpus=test_corpus_name, language=language, type='subset')

print 'Generating subset model word2vec features. This will take a while.'
simple_word2vec_features.generate_feature(corpus=subset_train_corpus_name,
                          test_corpus=test_corpus_name, language=language, type='subset')


print 'Generating subset model doc2vec features'
simple_doc2vec_features.generate_features(corpus=full_train_corpus_name,
                                          test_corpus=test_corpus_name, language=language, type='full')

print 'Generating subset model word2vec features. This will take a while.'
simple_word2vec_features.generate_feature(corpus=subset_train_corpus_name,
                          test_corpus=test_corpus_name, language=language, type='full')

print 'Getting subsample of features for 50% classifier input.'

full_word2vec_path = '_'.join([language,'full', 'word2vec_train_features.tsv'])
full_doc2vec_path = '_'.join([language,'full', 'doc2vec_train_features.tsv'])


classifier_sample.classifier_subset(file_name=full_word2vec_path, seed=123, split_ratio=.5)
classifier_sample.classifier_subset(file_name=full_doc2vec_path, seed=123, split_ratio=.5)