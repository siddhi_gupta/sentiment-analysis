__author__ = 'joe'

import sample_data
import generate_features
import os
import gensim


if __name__ == "__main__":


    print 'Recode, split, and sample data------------------------------------------------\n\n'


    print '\n\nRecode Arabic data to binary DV\n\n'
    sample_data.recode_data(file_path='reviews.tsv', base_path=os.getcwd())

    print '\n\nSplit data into train/test sets\n\n'
    sample_data.train_test_split_indices(data_path='binary_arabic_reviews.tsv', file_name_prefix='arabic_reviews', to_test=3000)

    print '\n\nTake subset of train data\n\n'
    sample_data.subset_indices(data_path='arabic_reviews_train_indices.txt', subset_n=10000,
                   file_name_prefix='arabic_reviews', seed=123)


    print '\n\nGenerate word2vec and doc2vec feature sets------------------------------------------------\n\n'


    ids_path = 'arabic_reviews_subset_10000.txt'
    reviews_path = 'binary_arabic_reviews.tsv'


    print '\n\nBuilding word2vec features\n\n'
    generate_features.build_word2vec_features(ids_path=ids_path, reviews_path=reviews_path, naming_prefix='arabic_reviews',
                            save_model=False)


    print '\n\nBuilding doc2vec features\n\n'

    generate_features.build_doc2vec_features(ids_path=ids_path, reviews_path=reviews_path, naming_prefix='arabic_reviews',
                            save_model=True)


    print '\n\nInfer features for unseen test data------------------------------------------------\n\n'

    # Set paths
    outpath = os.getcwd() + '/arabic_reviews_10000_doc2vec_inferred_training_features.tsv'
    ids_to_infer = ids_path = ids_path = 'arabic_reviews_test_indices.txt'

    print '\n\nLoading model\n\n'
    model = gensim.models.Doc2Vec.load('arabic_reviews_10000.doc2vec')

    # Initiate doc2vec_document_features instance
    infer_features = generate_features.doc2vec_document_features(ids_path=ids_path, reviews_path=reviews_path,
                                               output_path=outpath, model=model)

    print '\n\nInferring features for unseen data\n\n'
    generate_features.doc2vec_document_features.generate_inferred_features(infer_features)

    print '\n\nFinished\n\n'




else:


    # Recode, split, and sample data ----------------

    # Recode Arabic data to binary DV
    sample_data.recode_data(file_path='reviews.tsv', base_path=os.getcwd())

    # Split data into train/test sets
    sample_data.train_test_split_indices(data_path='binary_arabic_reviews.tsv', file_name_prefix='arabic_reviews', to_test=3000)

    # Take subset of train data
    sample_data.subset_indices(data_path='arabic_reviews_train_indices.txt', subset_n=10000,
                   file_name_prefix='arabic_reviews', seed=123)


    # Generate word2vec and doc2vec feature sets ------------------


    ids_path = 'arabic_reviews_subset_10000.txt'
    reviews_path = 'binary_arabic_reviews.tsv'


    # Build word2vec features
    generate_features.build_word2vec_features(ids_path=ids_path, reviews_path=reviews_path, naming_prefix='arabic_reviews',
                            save_model=False)


    # Build doc2vec features

    generate_features.build_doc2vec_features(ids_path=ids_path, reviews_path = reviews_path, naming_prefix='arabic_reviews',
                            save_model=True)


    #INFER FEATURES

    # Set paths
    outpath = os.getcwd() + '/arabic_reviews_10000_doc2vec_inferred_training_features.tsv'
    ids_to_infer = ids_path = ids_path = 'arabic_reviews_test_indices.txt'

    # Load model
    model = gensim.models.Doc2Vec.load('arabic_reviews_10000.doc2vec')

    # Initiate doc2vec_document_features instance
    infer_features = generate_features.doc2vec_document_features(ids_path=ids_path, reviews_path=reviews_path,
                                               output_path=outpath, model=model)

    # Infer features for unseen data
    generate_features.doc2vec_document_features.generate_inferred_features(infer_features)
