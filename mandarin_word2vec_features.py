__author__ = 'joe'

import gensim
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec
import gensim.models.doc2vec
import multiprocessing
from random import shuffle
import datetime
import csv
import sys
import numpy as np

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# corpus = sys.argv[1]
# test_corpus = sys.argv[2]
# language = sys.argv[3]
# type = sys.argv[4]

dimensions = 300
#
# language = 'arabic'
# type = 'new_full'
#
# corpus = 'arabic_full_train_corpus.tsv'
# test_corpus = 'arabic_test_corpus.tsv'


#
# language = 'english'
# type = 'new_full'
#
# corpus = '/Users/joe/Google Drive/PycharmProjects/anlp_project/sentiment-analysis/document_corpora/english_full_train_corpus.tsv'
# test_corpus = '/Users/joe/Google Drive/PycharmProjects/anlp_project/sentiment-analysis/document_corpora/english_test_corpus.tsv'
#
#

def make_agg_vec(doc, num_features, model_word_set, model):

    feature_vec = np.zeros((num_features,), dtype="float32")

    nwords = 0.

    for word in list(gensim.utils.to_unicode(doc[2]).split()[0]):
        if word in model_word_set:
            nwords += 1
            feature_vec = np.add(feature_vec, model[word])

    avg_feature_vec = feature_vec / nwords

    return avg_feature_vec

def generate_feature(corpus, test_corpus, language, type, dimensions=300):

    outpath = '_'.join([language,type, 'doc2vec_train_features.tsv'])

    test_out = '_'.join([language,type, 'doc2vec_test_features.tsv'])

    with open(corpus, 'rb') as f:



        all_data = csv.reader(f, delimiter='\t')
        all_docs = []

        for line in all_data:

            words = list(gensim.utils.to_unicode(line[2]).split()[0])
            all_docs.append(words)

    print 'finished getting docs'
    doc_list = all_docs[:]


    cores = multiprocessing.cpu_count()

    model = gensim.models.Word2Vec(doc_list, size=dimensions, min_count=5, workers=cores)
    num_features = model.layer1_size
    model_word_set = model.index2word



    print 'Writing training features to file'
    with open(outpath, 'wb') as output_file, open(corpus, 'rb') as input_file:

        writer = csv.writer(output_file, delimiter='\t')
        reader = csv.reader(input_file, delimiter='\t')

        counter = 0
        counter2 = 0

        for line in reader:
            row = [line[1]]
            row += make_agg_vec(doc=line[2], num_features=num_features,
                                model_word_set=model_word_set, model=model)
            writer.writerow(row)
            counter +=1
            if counter == 500:
                counter2 +=1
                print counter*counter2
                counter = 0

    print 'finished writing: {0}'.format(output_file)


    with open(test_corpus, 'rb') as input_file, open(test_out, 'wb') as output_file:

        writer = csv.writer(output_file)
        reader = csv.reader(input_file, delimiter='\t')

        counter = 0
        counter2 = 0

        for line in reader:
            row = [line[1]]
            row += make_agg_vec(doc=line[2], num_features=num_features,
                                model_word_set=model_word_set, model=model)
            writer.writerow(row)
            counter +=1
            if counter == 500:
                counter2 +=1
                print counter*counter2
                counter = 0





