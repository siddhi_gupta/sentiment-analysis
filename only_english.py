from langdetect import detect
f = open('English_Reviews.csv','r')
f1 = open('English_Reviews_latest.txt','w')

data = f.readlines()

for i in data:
	content = i.split(",")
	rating = content[0]
	del content[0]
	review = ','.join(content)
	review = review.replace('\n','').replace('\t','')
	try:
		if detect(review) == u'en':
			f1.write(rating+"\t"+review+"\n")
	except:
		pass

f.close()
f1.close()